import UploadImg from './src/UploadImg.vue'
import UploadImgs from './src/UploadImgs.vue'
import UploadFile from './src/UploadFile.vue'
import MyUploadVideo from './src/MyUploadVideo.vue'

export { UploadImg, UploadImgs, UploadFile, MyUploadVideo }
