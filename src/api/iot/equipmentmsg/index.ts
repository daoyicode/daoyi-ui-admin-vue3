import request from '@/config/axios'

// 设备消息 VO
export interface EquipmentMsgVO {
  id: number // 编号
  equipmentId: number // 设备编号
  equipmentName: string // 消息键
  msgName: string // 消息键
  msgKey: string // 消息键
  msgValue: string // 消息值
  alarmStatus: number // 报警状态
  msgTime: Date // 消息时间
}

// 设备消息 API
export const EquipmentMsgApi = {
  // 查询设备消息分页
  getEquipmentMsgPage: async (params: any) => {
    return await request.get({ url: `/iot/equipment-msg/page`, params })
  },

  // 查询设备消息详情
  getEquipmentMsg: async (id: number) => {
    return await request.get({ url: `/iot/equipment-msg/get?id=` + id })
  },

  // 新增设备消息
  createEquipmentMsg: async (data: EquipmentMsgVO) => {
    return await request.post({ url: `/iot/equipment-msg/create`, data })
  },

  // 修改设备消息
  updateEquipmentMsg: async (data: EquipmentMsgVO) => {
    return await request.put({ url: `/iot/equipment-msg/update`, data })
  },

  // 删除设备消息
  deleteEquipmentMsg: async (id: number) => {
    return await request.delete({ url: `/iot/equipment-msg/delete?id=` + id })
  },

  // 导出设备消息 Excel
  exportEquipmentMsg: async (params) => {
    return await request.download({ url: `/iot/equipment-msg/export-excel`, params })
  }
}
