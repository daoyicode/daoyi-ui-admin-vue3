import request from '@/config/axios'

// 会员设备场所 VO
export interface UserEquipmentPlaceVO {
  id: number // 编号
  equipmentPlaceName: string // 场所名称
  memberUserId: number // 会员编号
}

// 会员设备场所 API
export const UserEquipmentPlaceApi = {
  // 查询会员设备场所分页
  getUserEquipmentPlacePage: async (params: any) => {
    return await request.get({ url: `/iot/user-equipment-place/page`, params })
  },

  // 查询会员设备场所详情
  getUserEquipmentPlace: async (id: number) => {
    return await request.get({ url: `/iot/user-equipment-place/get?id=` + id })
  },

  // 新增会员设备场所
  createUserEquipmentPlace: async (data: UserEquipmentPlaceVO) => {
    return await request.post({ url: `/iot/user-equipment-place/create`, data })
  },

  // 修改会员设备场所
  updateUserEquipmentPlace: async (data: UserEquipmentPlaceVO) => {
    return await request.put({ url: `/iot/user-equipment-place/update`, data })
  },

  // 删除会员设备场所
  deleteUserEquipmentPlace: async (id: number) => {
    return await request.delete({ url: `/iot/user-equipment-place/delete?id=` + id })
  },

  // 导出会员设备场所 Excel
  exportUserEquipmentPlace: async (params) => {
    return await request.download({ url: `/iot/user-equipment-place/export-excel`, params })
  }
}
