import request from '@/config/axios'

// kafka原始消息 VO
export interface KafkaMsgLogsVO {
  id: number // 编号
  topics: string // 主题
  bootstrapServers: string // 生产者
  consumerGroupId: string // 消费者
  msg: string // 原始消息
  viewed: boolean // 是否已读
}

// kafka原始消息 API
export const KafkaMsgLogsApi = {
  // 查询kafka原始消息分页
  getKafkaMsgLogsPage: async (params: any) => {
    return await request.get({ url: `/iot/kafka-msg-logs/page`, params })
  },

  // 查询kafka原始消息详情
  getKafkaMsgLogs: async (id: number) => {
    return await request.get({ url: `/iot/kafka-msg-logs/get?id=` + id })
  },

  // 新增kafka原始消息
  createKafkaMsgLogs: async (data: KafkaMsgLogsVO) => {
    return await request.post({ url: `/iot/kafka-msg-logs/create`, data })
  },

  // 修改kafka原始消息
  updateKafkaMsgLogs: async (data: KafkaMsgLogsVO) => {
    return await request.put({ url: `/iot/kafka-msg-logs/update`, data })
  },

  // 删除kafka原始消息
  deleteKafkaMsgLogs: async (id: number) => {
    return await request.delete({ url: `/iot/kafka-msg-logs/delete?id=` + id })
  },

  // 导出kafka原始消息 Excel
  exportKafkaMsgLogs: async (params) => {
    return await request.download({ url: `/iot/kafka-msg-logs/export-excel`, params })
  }
}
