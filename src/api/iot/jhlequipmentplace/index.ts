import request from '@/config/axios'

// 精华隆场所信息 VO
export interface JhlEquipmentPlaceVO {
  id: number // 编号
  level: number // 等级
  mainCode: string // 场所代码
  placeName: string // 场所名称
  parentId: number // 父级编号
}

// 精华隆场所信息 API
export const JhlEquipmentPlaceApi = {
  // 查询精华隆场所信息分页
  getJhlEquipmentPlacePage: async (params: any) => {
    return await request.get({ url: `/iot/jhl-equipment-place/page`, params })
  },

  // 查询精华隆场所信息详情
  getJhlEquipmentPlace: async (id: number) => {
    return await request.get({ url: `/iot/jhl-equipment-place/get?id=` + id })
  },

  // 新增精华隆场所信息
  createJhlEquipmentPlace: async (data: JhlEquipmentPlaceVO) => {
    return await request.post({ url: `/iot/jhl-equipment-place/create`, data })
  },

  // 修改精华隆场所信息
  updateJhlEquipmentPlace: async (data: JhlEquipmentPlaceVO) => {
    return await request.put({ url: `/iot/jhl-equipment-place/update`, data })
  },

  // 同步精华隆场所信息
  asyncJhlEquipmentPlace: async () => {
    return await request.put({ url: `/iot/jhl-equipment-place/async` })
  },

  // 删除精华隆场所信息
  deleteJhlEquipmentPlace: async (id: number) => {
    return await request.delete({ url: `/iot/jhl-equipment-place/delete?id=` + id })
  },

  // 导出精华隆场所信息 Excel
  exportJhlEquipmentPlace: async (params) => {
    return await request.download({ url: `/iot/jhl-equipment-place/export-excel`, params })
  }
}
