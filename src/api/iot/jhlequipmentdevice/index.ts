import request from '@/config/axios'

// 精华隆设备信息 VO
export interface JhlEquipmentDeviceVO {
  id: number // 设备ID
  armStatus: number // 布撤防状态
  deviceType: string // 设备类型
  deviceIccid: number // 卡号
  onlineStatus: number // 在线状态
  placeId: number // 场所ID
  deviceName: string // 设备名称
  parentId: number // 父设备ID
  reportType: number // 协议类型
  mainCode: string // 公司主域
  lastOfflineTime: Date // 离线时间
  deviceIdentifier: string // 设备序列号
  mac: string // MAC
  subCode: number // 公司子域
  lastOnlineTime: Date // 上线时间
}

// 精华隆设备信息 API
export const JhlEquipmentDeviceApi = {
  // 查询精华隆设备信息分页
  getJhlEquipmentDevicePage: async (params: any) => {
    return await request.get({ url: `/iot/jhl-equipment-device/page`, params })
  },

  // 查询精华隆设备信息详情
  getJhlEquipmentDevice: async (id: number) => {
    return await request.get({ url: `/iot/jhl-equipment-device/get?id=` + id })
  },

  // 新增精华隆设备信息
  createJhlEquipmentDevice: async (data: JhlEquipmentDeviceVO) => {
    return await request.post({ url: `/iot/jhl-equipment-device/create`, data })
  },

  // 修改精华隆设备信息
  updateJhlEquipmentDevice: async (data: JhlEquipmentDeviceVO) => {
    return await request.put({ url: `/iot/jhl-equipment-device/update`, data })
  },

  // 同步精华隆设备信息
  asyncJhlEquipmentDevice: async () => {
    return await request.put({ url: `/iot/jhl-equipment-device/async` })
  },

  // 删除精华隆设备信息
  deleteJhlEquipmentDevice: async (id: number) => {
    return await request.delete({ url: `/iot/jhl-equipment-device/delete?id=` + id })
  },

  // 导出精华隆设备信息 Excel
  exportJhlEquipmentDevice: async (params) => {
    return await request.download({ url: `/iot/jhl-equipment-device/export-excel`, params })
  }
}
