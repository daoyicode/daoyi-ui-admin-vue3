import request from '@/config/axios'

// 会员设备 VO
export interface UserEquipmentVO {
  id: number // 编号
  equipmentManufacturer: number // 设备厂商
  equipmentId: number // 设备编号
  placeId: number // 场所编号
  memberUserId: number // 会员编号
}

// 会员设备 API
export const UserEquipmentApi = {
  // 查询会员设备分页
  getUserEquipmentPage: async (params: any) => {
    return await request.get({ url: `/iot/user-equipment/page`, params })
  },

  // 查询会员设备详情
  getUserEquipment: async (id: number) => {
    return await request.get({ url: `/iot/user-equipment/get?id=` + id })
  },

  // 新增会员设备
  createUserEquipment: async (data: UserEquipmentVO) => {
    return await request.post({ url: `/iot/user-equipment/create`, data })
  },

  // 修改会员设备
  updateUserEquipment: async (data: UserEquipmentVO) => {
    return await request.put({ url: `/iot/user-equipment/update`, data })
  },

  // 删除会员设备
  deleteUserEquipment: async (id: number) => {
    return await request.delete({ url: `/iot/user-equipment/delete?id=` + id })
  },

  // 导出会员设备 Excel
  exportUserEquipment: async (params) => {
    return await request.download({ url: `/iot/user-equipment/export-excel`, params })
  }
}
