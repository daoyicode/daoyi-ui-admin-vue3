import request from '@/config/axios'

// 文章 VO
export interface ArticleDetailVO {
  id: number // 编号
  appSiteId: number // 站点编号
  appId: string // 站点标识
  categoryId: number // 栏目编号
  title: string // 标题
  subTitle: string // 副标题
  mainPic: string // 主图
  articlePhotos: string // 文章图册
  articleShortDesc: string // 简要
  articleContent: string // 内容
  status: number // 文章状态
  viewNum: number // 点击量
  sort: number // 排序
}

// 文章 API
export const ArticleDetailApi = {
  // 查询文章分页
  getArticleDetailPage: async (params: any) => {
    return await request.get({ url: `/cms/article-detail/page`, params })
  },

  // 查询文章详情
  getArticleDetail: async (id: number) => {
    return await request.get({ url: `/cms/article-detail/get?id=` + id })
  },

  // 新增文章
  createArticleDetail: async (data: ArticleDetailVO) => {
    return await request.post({ url: `/cms/article-detail/create`, data })
  },

  // 修改文章
  updateArticleDetail: async (data: ArticleDetailVO) => {
    return await request.put({ url: `/cms/article-detail/update`, data })
  },

  // 删除文章
  deleteArticleDetail: async (id: number) => {
    return await request.delete({ url: `/cms/article-detail/delete?id=` + id })
  },

  // 导出文章 Excel
  exportArticleDetail: async (params) => {
    return await request.download({ url: `/cms/article-detail/export-excel`, params })
  }
}
