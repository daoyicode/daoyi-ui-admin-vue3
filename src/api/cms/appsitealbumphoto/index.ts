import request from '@/config/axios'

// 相册照片 VO
export interface AppSiteAlbumPhotoVO {
  id: number // 编号
  appSiteId: number // 站点编号
  appId: string // 站点标识
  albumId: number // 相册编号
  description: string // 描述
  url: string // 照片
  status: number // 状态
  sort: number // 序号
  viewNum: number // 点击量
  thumbsUpTimes: number // 点赞数量
}

// 相册照片 API
export const AppSiteAlbumPhotoApi = {
  // 查询相册照片分页
  getAppSiteAlbumPhotoPage: async (params: any) => {
    return await request.get({ url: `/cms/app-site-album-photo/page`, params })
  },

  // 查询相册照片详情
  getAppSiteAlbumPhoto: async (id: number) => {
    return await request.get({ url: `/cms/app-site-album-photo/get?id=` + id })
  },

  // 新增相册照片
  createAppSiteAlbumPhoto: async (data: AppSiteAlbumPhotoVO) => {
    return await request.post({ url: `/cms/app-site-album-photo/create`, data })
  },

  // 修改相册照片
  updateAppSiteAlbumPhoto: async (data: AppSiteAlbumPhotoVO) => {
    return await request.put({ url: `/cms/app-site-album-photo/update`, data })
  },

  // 删除相册照片
  deleteAppSiteAlbumPhoto: async (id: number) => {
    return await request.delete({ url: `/cms/app-site-album-photo/delete?id=` + id })
  },

  // 导出相册照片 Excel
  exportAppSiteAlbumPhoto: async (params) => {
    return await request.download({ url: `/cms/app-site-album-photo/export-excel`, params })
  }
}
