import request from '@/config/axios'

// 阅读记录 VO
export interface ArticleDetailReadLogsVO {
  id: number // 编号
  articleId: number // 文章编号
  userId: number // 用户编号
  beginTime: Date // 开始时间
  endTime: Date // 结束时间
}

// 阅读记录 API
export const ArticleDetailReadLogsApi = {
  // 查询阅读记录分页
  getArticleDetailReadLogsPage: async (params: any) => {
    return await request.get({ url: `/cms/article-detail-read-logs/page`, params })
  },

  // 查询阅读记录详情
  getArticleDetailReadLogs: async (id: number) => {
    return await request.get({ url: `/cms/article-detail-read-logs/get?id=` + id })
  },

  // 新增阅读记录
  createArticleDetailReadLogs: async (data: ArticleDetailReadLogsVO) => {
    return await request.post({ url: `/cms/article-detail-read-logs/create`, data })
  },

  // 修改阅读记录
  updateArticleDetailReadLogs: async (data: ArticleDetailReadLogsVO) => {
    return await request.put({ url: `/cms/article-detail-read-logs/update`, data })
  },

  // 删除阅读记录
  deleteArticleDetailReadLogs: async (id: number) => {
    return await request.delete({ url: `/cms/article-detail-read-logs/delete?id=` + id })
  },

  // 导出阅读记录 Excel
  exportArticleDetailReadLogs: async (params) => {
    return await request.download({ url: `/cms/article-detail-read-logs/export-excel`, params })
  }
}
