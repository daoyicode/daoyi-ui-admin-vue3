import request from '@/config/axios'

// 站点评论 VO
export interface AppSiteCommentVO {
  id: number // 编号
  userType: number // 用户类型
  userId: number // 用户编号
  userNickname: string // 用户昵称
  appSiteId: number // 站点编号
  appId: string // 站点标识
  categoryId: number // 栏目编号
  articleId: number // 文章编号
  parentId: number // 父级编号
  commentContent: string // 内容
  status: number // 评论状态
  approveStatus: number // 审核状态
  sort: number // 排序
  creatorIp: string // 创建者ip
  creatorCity: string // 创建者城市
}

// 站点评论 API
export const AppSiteCommentApi = {
  // 查询站点评论分页
  getAppSiteCommentPage: async (params: any) => {
    return await request.get({ url: `/cms/app-site-comment/page`, params })
  },

  // 查询站点评论详情
  getAppSiteComment: async (id: number) => {
    return await request.get({ url: `/cms/app-site-comment/get?id=` + id })
  },

  // 新增站点评论
  createAppSiteComment: async (data: AppSiteCommentVO) => {
    return await request.post({ url: `/cms/app-site-comment/create`, data })
  },

  // 修改站点评论
  updateAppSiteComment: async (data: AppSiteCommentVO) => {
    return await request.put({ url: `/cms/app-site-comment/update`, data })
  },

  // 修改站点评论状态
  updateAppSiteCommentStatus: async (data: AppSiteCommentVO) => {
    return await request.put({ url: `/cms/app-site-comment/update-status`, data })
  },

  // 修改站点评论审核状态
  updateAppSiteCommentApproveStatus: async (data: AppSiteCommentVO) => {
    return await request.put({ url: `/cms/app-site-comment/update-approve-status`, data })
  },

  // 删除站点评论
  deleteAppSiteComment: async (id: number) => {
    return await request.delete({ url: `/cms/app-site-comment/delete?id=` + id })
  },

  // 导出站点评论 Excel
  exportAppSiteComment: async (params) => {
    return await request.download({ url: `/cms/app-site-comment/export-excel`, params })
  }
}
