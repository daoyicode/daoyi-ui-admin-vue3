import request from '@/config/axios'

// 站点相册 VO
export interface AppSiteAlbumVO {
  id: number // 编号
  appSiteId: number // 站点编号
  appId: string // 站点标识
  albumName: string // 名称
  description: string // 描述
  albumCover: string // 封面
  status: number // 状态
  viewNum: number // 点击量
  thumbsUpTimes: number // 点赞数量
}

// 站点相册 API
export const AppSiteAlbumApi = {
  // 查询站点相册分页
  getAppSiteAlbumPage: async (params: any) => {
    return await request.get({ url: `/cms/app-site-album/page`, params })
  },

  // 查询站点相册详情
  getAppSiteAlbum: async (id: number) => {
    return await request.get({ url: `/cms/app-site-album/get?id=` + id })
  },

  // 新增站点相册
  createAppSiteAlbum: async (data: AppSiteAlbumVO) => {
    return await request.post({ url: `/cms/app-site-album/create`, data })
  },

  // 修改站点相册
  updateAppSiteAlbum: async (data: AppSiteAlbumVO) => {
    return await request.put({ url: `/cms/app-site-album/update`, data })
  },

  // 删除站点相册
  deleteAppSiteAlbum: async (id: number) => {
    return await request.delete({ url: `/cms/app-site-album/delete?id=` + id })
  },

  // 导出站点相册 Excel
  exportAppSiteAlbum: async (params) => {
    return await request.download({ url: `/cms/app-site-album/export-excel`, params })
  }
}
