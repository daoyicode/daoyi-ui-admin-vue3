import request from '@/config/axios'

// 站点足迹 VO
export interface AppSiteFootprintVO {
  id: number // 编号
  userType: number // 用户类型
  userId: number // 用户编号
  userNickname: string // 用户昵称
  userAvatar: string // 用户头像
  appSiteId: number // 站点编号
  appId: string // 站点标识
  categoryId: number // 栏目编号
  articleId: number // 文章编号
  talkId: number // 说说编号
  messageId: number // 留言编号
  commentId: number // 评论编号
  footprint: number // 足记
}

// 站点足迹 API
export const AppSiteFootprintApi = {
  // 查询站点足迹分页
  getAppSiteFootprintPage: async (params: any) => {
    return await request.get({ url: `/cms/app-site-footprint/page`, params })
  },

  // 查询站点足迹详情
  getAppSiteFootprint: async (id: number) => {
    return await request.get({ url: `/cms/app-site-footprint/get?id=` + id })
  },

  // 新增站点足迹
  createAppSiteFootprint: async (data: AppSiteFootprintVO) => {
    return await request.post({ url: `/cms/app-site-footprint/create`, data })
  },

  // 修改站点足迹
  updateAppSiteFootprint: async (data: AppSiteFootprintVO) => {
    return await request.put({ url: `/cms/app-site-footprint/update`, data })
  },

  // 删除站点足迹
  deleteAppSiteFootprint: async (id: number) => {
    return await request.delete({ url: `/cms/app-site-footprint/delete?id=` + id })
  },

  // 导出站点足迹 Excel
  exportAppSiteFootprint: async (params) => {
    return await request.download({ url: `/cms/app-site-footprint/export-excel`, params })
  }
}
