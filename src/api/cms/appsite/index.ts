import request from '@/config/axios'

export interface AppSiteConfigItem {
  key: string
  type: number
  value: any
}

// 站点 VO
export interface AppSiteVO {
  id: number // 编号
  terminalType: number // 站点类型
  appId: string // 站点标识
  appName: string // 站点名称
  appLogoUrl: string // LOGO
  appPhotos: string // 站点相册
  appShortDesc: string // 站点简介
  appDescription: string // 站点介绍
  configInfoList: string // 站点配置,
  status: number
  approveStatus: number
}

// 站点 API
export const AppSiteApi = {
  // 查询站点分页
  getAppSitePage: async (params: any) => {
    return await request.get({ url: `/cms/app-site/page`, params })
  },

  // 查询站点详情
  getAppSite: async (id: number) => {
    return await request.get({ url: `/cms/app-site/get?id=` + id })
  },

  // 新增站点
  createAppSite: async (data: AppSiteVO) => {
    return await request.post({ url: `/cms/app-site/create`, data })
  },

  // 修改站点
  updateAppSite: async (data: AppSiteVO) => {
    return await request.put({ url: `/cms/app-site/update`, data })
  },

  // 修改站点状态
  updateAppSiteStatus: async (data: AppSiteVO) => {
    return await request.put({ url: `/cms/app-site/update-status`, data })
  },

  // 修改站点核状态
  updateAppSiteApproveStatus: async (data: AppSiteVO) => {
    return await request.put({ url: `/cms/app-site/update-approve-status`, data })
  },
  // 删除站点
  deleteAppSite: async (id: number) => {
    return await request.delete({ url: `/cms/app-site/delete?id=` + id })
  },

  // 导出站点 Excel
  exportAppSite: async (params) => {
    return await request.download({ url: `/cms/app-site/export-excel`, params })
  },

  // 获取标签
  getTagDictionary: async (appId: string, categoryId?: number) => {
    return await request.get({
      url: `/cms/app-site/getTagDictionary?appId=${appId}&categoryId=${categoryId}`
    })
  }
}
