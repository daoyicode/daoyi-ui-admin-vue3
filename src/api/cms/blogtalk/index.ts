import request from '@/config/axios'

// 说说 VO
export interface BlogTalkVO {
  id: number // 编号
  userType: number // 用户类型
  userId: number // 用户编号
  userNickname: string // 用户昵称
  userAvatar: string // 用户头像
  appSiteId: number // 站点编号
  appId: string // 站点标识
  talkImgList: string // 图片
  talkVideo: string // 视频
  content: string // 内容
  referTo: string // @谁
  status: number // 状态
  approveStatus: number // 审核状态
  sort: number // 排序
  viewTimes: number // 访问次数
  thumbsUpTimes: number // 点赞数量
  creatorIp: string // 创建者ip
  creatorCity: string // 创建者城市
}

// 说说 API
export const BlogTalkApi = {
  // 查询说说分页
  getBlogTalkPage: async (params: any) => {
    return await request.get({ url: `/cms/blog-talk/page`, params })
  },

  // 查询说说详情
  getBlogTalk: async (id: number) => {
    return await request.get({ url: `/cms/blog-talk/get?id=` + id })
  },

  // 新增说说
  createBlogTalk: async (data: BlogTalkVO) => {
    return await request.post({ url: `/cms/blog-talk/create`, data })
  },

  // 修改说说
  updateBlogTalk: async (data: BlogTalkVO) => {
    return await request.put({ url: `/cms/blog-talk/update`, data })
  },

  // 删除说说
  deleteBlogTalk: async (id: number) => {
    return await request.delete({ url: `/cms/blog-talk/delete?id=` + id })
  },

  // 导出说说 Excel
  exportBlogTalk: async (params) => {
    return await request.download({ url: `/cms/blog-talk/export-excel`, params })
  }
}
