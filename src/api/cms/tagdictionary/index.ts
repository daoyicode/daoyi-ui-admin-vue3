import request from '@/config/axios'

// 文章标签 VO
export interface TagDictionaryVO {
  id: number // 编号
  name: string // 标签名称
  articleCount: number // 文章数量
}

// 文章标签 API
export const TagDictionaryApi = {
  // 查询文章标签分页
  getTagDictionaryPage: async (params: any) => {
    return await request.get({ url: `/cms/tag-dictionary/page`, params })
  },

  // 查询文章标签详情
  getTagDictionary: async (id: number) => {
    return await request.get({ url: `/cms/tag-dictionary/get?id=` + id })
  },

  // 新增文章标签
  createTagDictionary: async (data: TagDictionaryVO) => {
    return await request.post({ url: `/cms/tag-dictionary/create`, data })
  },

  // 修改文章标签
  updateTagDictionary: async (data: TagDictionaryVO) => {
    return await request.put({ url: `/cms/tag-dictionary/update`, data })
  },

  // 删除文章标签
  deleteTagDictionary: async (id: number) => {
    return await request.delete({ url: `/cms/tag-dictionary/delete?id=` + id })
  },

  // 导出文章标签 Excel
  exportTagDictionary: async (params) => {
    return await request.download({ url: `/cms/tag-dictionary/export-excel`, params })
  }
}
