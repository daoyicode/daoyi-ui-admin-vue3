import request from '@/config/axios'

// 留言 VO
export interface BlogMessageVO {
  id: number // 编号
  appSiteId: number // 站点编号
  appId: string // 站点标识
  userType: number // 用户类型
  userId: number // 用户编号
  userNickname: string // 用户昵称
  userAvatar: string // 用户头像
  bgColor: string // 背景颜色
  bgUrl: string // 背景图片
  color: string // 字体颜色
  fontSize: number // 字体大小
  fontWeight: number // 字体宽度
  tag: string // 标签
  message: string // 内容
  referTo: string // @谁
  status: number // 状态
  approveStatus: number // 审核状态
  sort: number // 排序
  viewTimes: number // 访问次数
  thumbsUpTimes: number // 点赞数量
  creatorIp: string // 创建者ip
  creatorCity: string // 创建者城市
}

// 留言 API
export const BlogMessageApi = {
  // 查询留言分页
  getBlogMessagePage: async (params: any) => {
    return await request.get({ url: `/cms/blog-message/page`, params })
  },

  // 查询留言详情
  getBlogMessage: async (id: number) => {
    return await request.get({ url: `/cms/blog-message/get?id=` + id })
  },

  // 新增留言
  createBlogMessage: async (data: BlogMessageVO) => {
    return await request.post({ url: `/cms/blog-message/create`, data })
  },

  // 修改留言
  updateBlogMessage: async (data: BlogMessageVO) => {
    return await request.put({ url: `/cms/blog-message/update`, data })
  },

  // 删除留言
  deleteBlogMessage: async (id: number) => {
    return await request.delete({ url: `/cms/blog-message/delete?id=` + id })
  },

  // 导出留言 Excel
  exportBlogMessage: async (params) => {
    return await request.download({ url: `/cms/blog-message/export-excel`, params })
  }
}
