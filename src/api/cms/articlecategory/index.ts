import request from '@/config/axios'

// 文章栏目 VO
export interface ArticleCategoryVO {
  id: number // 编号
  appSiteId: number // 站点编号
  appId: string // 站点标识
  parentId: number // 父级栏目
  title: string // 栏目标题
  subTitle: string // 副标题
  iconUrl: string // 图标
  categoryPhotos: string // 栏目相册
  categoryShortDesc: string // 栏目简介
  categoryDescription: string // 栏目介绍
  navBar: boolean // 导航栏
  status: number // 栏目状态
  viewNum: number // 点击量
}

// 文章栏目 API
export const ArticleCategoryApi = {
  // 查询文章栏目分页
  getArticleCategoryPage: async (params: any) => {
    return await request.get({ url: `/cms/article-category/page`, params })
  },

  // 查询文章栏目详情
  getArticleCategory: async (id: number) => {
    return await request.get({ url: `/cms/article-category/get?id=` + id })
  },

  // 新增文章栏目
  createArticleCategory: async (data: ArticleCategoryVO) => {
    return await request.post({ url: `/cms/article-category/create`, data })
  },

  // 修改文章栏目
  updateArticleCategory: async (data: ArticleCategoryVO) => {
    return await request.put({ url: `/cms/article-category/update`, data })
  },

  // 删除文章栏目
  deleteArticleCategory: async (id: number) => {
    return await request.delete({ url: `/cms/article-category/delete?id=` + id })
  },

  // 导出文章栏目 Excel
  exportArticleCategory: async (params) => {
    return await request.download({ url: `/cms/article-category/export-excel`, params })
  }
}
